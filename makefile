md=$(shell ls *.md)
md_html=$(md:%.md=%.html)

all: $(md_html)

%.html: %.md styles.css
	pandoc --from markdown --to html -s $< --css styles.css -o $@

# special rule for debugging variable names in this makefile
print-%:
	@echo '$*=$($*)'
