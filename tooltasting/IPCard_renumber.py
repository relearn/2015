from __future__ import print_function
from argparse import ArgumentParser
import sys, re

p = ArgumentParser("")
p.add_argument("--pattern", default=r"(10.9.8.)(\d+)")
p.add_argument("--oldnumber", type=int, default=50)
p.add_argument("--newnumber", type=int, default=100)
args = p.parse_args()

pat = re.compile(args.pattern)
src = sys.stdin.read()

def repl (m):
	m.group(1)
	# print(m, file=sys.stderr)
	# print ((m.groups(), m.group(0), m.group(1), m.group(2)), file=sys.stderr)
	o = int(m.group(2))
	n = args.newnumber + (o - args.oldnumber)
	ret = m.group(1)+str(n)
	print ("Replacing '{0}' with '{1}'".format(m.group(0), ret), file=sys.stderr)
	return ret

print (pat.sub(repl, src))